#!/bin/bash
set -e

echo "********************* STARTING TO BUILD ACQUIA COMMONS *********************"
if [ "${BUILDTYPE}" == "production" ]
then
echo "Launching a production build task"
drush make /vagrant/commons_files/build-commons.make --no-cache --working-copy --tar ${OUTPUTDIR}/commons-${SELECTED_BRANCH}
else
echo "Launching a development build task"
drush make /vagrant/commons_files/build-commons-dev.make --no-cache --working-copy --tar ${OUTPUTDIR}/commons_nightly_dev_${SELECTED_BRANCH}_`date +%s`
fi
echo "********************* FINISHED THE BUILD SUCCESSFULLY *********************"