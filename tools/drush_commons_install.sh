#!/bin/bash
set -e

# If we don't disable the example content, commons currently starts sending emails to the example emails:
# http://drupal.org/node/1855548
# Also: unchecking checkboxes is broken in drush site-install:
# http://drupal.org/node/1247338
# And removing the example content: http://drupal.org/node/1906678
sudo -u www-data drush site-install --db-url=mysql://commons:commons@localhost/commons --site-name=QASite --account-name=admin --account-pass=commons --account-mail=admin@example.com --site-mail=site@example.com -v -y commons commons_anonymous_welcome_text_form.commons_anonymous_welcome_title="Oh hai" commons_anonymous_welcome_text_form.commons_anonymous_welcome_body="No shirts, no shoes, no service." commons_create_first_group.commons_first_group_title="Internet People" commons_create_first_group.commons_first_group_body="This is the first group on the page."
