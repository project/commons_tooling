maintainer       "Marc Seeger, Acquia"
maintainer_email "marc.seeger@acquia.com"
license          "GNU General Public License, version 2"
description      "Installs/Configures Commons"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.1.0"
