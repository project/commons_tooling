#
# Cookbook Name:: Commons
# Recipe:: default
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

execute "disable-default-site" do
  command "a2dissite default"
end

# This is really just an ugly ugly hack
# We should fix this properly, but moving to vagrant-ariadne
# will probably be a better solution
execute "replace_apache_php_ini" do
  command "rm /etc/php5/apache2/php.ini; cp /etc/php5/cgi/php.ini /etc/php5/apache2/php.ini; sed -i 's/memory_limit = .*/memory_limit = 384M/' /etc/php5/apache2/php.ini; service apache2 restart"
end

web_app "commons" do
  application_name "commons"
  docroot "/vagrant/commons_files"
end

mysql_connection_info = {:host => "localhost", :username => 'root', :password => node['mysql']['server_root_password']}


# Just in case it still exists from a previous run, get rid of it
mysql_database "commons" do
  connection mysql_connection_info
  action :drop
end

# Create a database named 'commons'
mysql_database 'commons' do
  connection mysql_connection_info
  action :create
end

# Create a mysql user. Doesn't have any privileges
mysql_database_user 'commons' do
  connection mysql_connection_info
  password 'commons'
  action :create
end

# Grant all privileges
mysql_database_user 'commons' do
  connection mysql_connection_info
  database_name 'commons'
  action :grant
end