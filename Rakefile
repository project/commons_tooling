require 'fileutils'
require 'rubygems'
require 'bundler/setup'

# Directory names
ROOT_DIR = File.dirname(__FILE__)
WORKSPACE_DIRECTORY = File.join(ROOT_DIR, "vm_workspace")
puts "Workspace directory: #{WORKSPACE_DIRECTORY}"
COMMONS_CHECKOUT = File.join(ROOT_DIR, 'commons_files') # This is also present in the commons chef recipe. Change it here, change it there.
puts "Directory for the commons checkout/docroot: #{COMMONS_CHECKOUT}"
ARTIFACTS_DIR = File.join(ROOT_DIR, 'artifacts')

def checkout_commons(branch = '7.x-3.x', output_dir = COMMONS_CHECKOUT)
  clean_directory(output_dir)
  puts "Checking out commons branch '#{branch}' to '#{output_dir}'."
  system("git clone --branch #{branch} --depth 1 git://git.drupal.org/project/commons.git #{output_dir}")
end

def clean_directory(dir_name)
  puts "Cleaning up. Deleting all files in directory '#{dir_name}'."
  # A lame try to protect against accidentally deleting something like '/'
  unless File.expand_path(dir_name).include?(File.dirname(__FILE__))
    puts "The '#{dir_name}' directory should be in a subdirectory relative to this Rakefile."
    exit(1)
  end
  Dir.foreach(dir_name) {|dir_entry| next if ['..','.'].include?(dir_entry); puts "Deleting: #{dir_name}/#{dir_entry}"; FileUtils.rm_rf("#{dir_name}/#{dir_entry}") }
  puts "Cleanup of '#{dir_name}' done."
end

def build_task(build_type, selected_branch = '7.x-3.x')
  build_result = false
  begin
    Rake::Task["boot_build_vm"].execute
    checkout_commons(selected_branch)
    Dir.chdir(WORKSPACE_DIRECTORY) do
      build_result = system("vagrant ssh -c 'OUTPUTDIR=/vagrant/artifacts BUILDTYPE=#{build_type} SELECTED_BRANCH=#{selected_branch} /vagrant/tools/commons_tarball_build.sh'")
      puts "Build tarball script successful: #{build_result}"
    end
    # Our local directory
    FileUtils.mkdir(ARTIFACTS_DIR) unless File.exist?(ARTIFACTS_DIR)
    FileUtils.cp_r(Dir["#{WORKSPACE_DIRECTORY}/artifacts/*"], ARTIFACTS_DIR)
  ensure
    Rake::Task["terminate_and_delete_vm"].execute
    clean_directory(WORKSPACE_DIRECTORY)
  end
  build_result
end

def boot_vm(vagrant_file)
  clean_directory(WORKSPACE_DIRECTORY)
  Dir.mkdir(COMMONS_CHECKOUT) unless File.exist?(COMMONS_CHECKOUT)
  FileUtils.cp_r(vagrant_file, "#{WORKSPACE_DIRECTORY}/Vagrantfile")
  Dir.chdir(WORKSPACE_DIRECTORY) do
    # We will try to reload a failed boot 3 times. After that we'll just give up.
    result = system("vagrant up") || system("vagrant reload") || system("vagrant reload") || system("vagrant reload")
    exit(1) unless result
  end
end

desc 'Will terminate and delete the currently running VM'
task :terminate_and_delete_vm do
  Dir.chdir(WORKSPACE_DIRECTORY) { system("vagrant destroy -f") }
end

desc 'Will shut down the currently running VM'
task :shutdown_vm do
  Dir.chdir(WORKSPACE_DIRECTORY) { system("vagrant halt") }
end

desc 'Will launch a vagrant/virtualbox VM and install a LAMP stack on it'
task :boot_lamp_vm do
  puts "Booting LAMP VM"
  boot_vm('tools/Vagrantfile_test')
end

desc 'Will launch a vagrant/virtualbox VM and install everything needed to build the commons makefile on it'
task :boot_build_vm do
  puts "Booting makefile build VM"
  boot_vm('tools/Vagrantfile_build')
end

desc 'Will take a previously launched VM and use the files (in "commons_files") to install commons on the VM. You can also set the COMMONS_TARBALL env variable to use a specific tarball. This will overwrite old installations on the VM.'
task :install_commons_on_lamp_vm do
  if ENV["COMMONS_TARBALL"]
    unless File.exist?(ENV["COMMONS_TARBALL"])
      puts "Specified tarball '#{ENV["COMMONS_TARBALL"]}' doesn't exist."
      exit(1)
    end
    puts "We have a tarball! Cleaning up current commons dir ('#{COMMONS_CHECKOUT}')"
    clean_directory(COMMONS_CHECKOUT)
    extract_commandline = "tar xfz #{ENV["COMMONS_TARBALL"]} -C #{COMMONS_CHECKOUT}/ --strip 1"
    puts "Extracting the tarball: #{extract_commandline}"
    system(extract_commandline)
    puts "Done extracting the tarball."
  end
  if File.exist?("#{COMMONS_CHECKOUT}/index.php") && File.exist?("#{COMMONS_CHECKOUT}/.htaccess")
    puts "Found a commons install in '#{COMMONS_CHECKOUT}'. Starting installation."
    success = false
    Dir.chdir(WORKSPACE_DIRECTORY) do
      success = system("vagrant ssh -c 'cd /vagrant/#{File.basename(COMMONS_CHECKOUT)}; /vagrant/tools/drush_commons_install.sh'")
    end
    if success
      puts "Installation succeeded"
    else
      puts "Installation failed :("
      exit(2)
    end
  else
    puts "Can't find a commons installation in '#{File.expand_path(WORKSPACE_DIRECTORY)}'. We need something to install."
    exit(3)
  end
end

desc 'Copy the little PHP backdoor into the default commons docroot on the VM'
task :install_qa_backdoor_into_vm do
  puts "Injecting the qa backdoor into the commons docroot on the VM"
  FileUtils.cp('tools/qa_backdoor.php', 'commons_files/')
  puts "Done injecting the qa backdoor"
end

desc 'Build a production tarball from the commons git repo (takes care of VM setup)'
task :build_commons_production do
  result = build_task('production')
  raise "Building the commons production tarball failed. Please check your logs." unless result
end

desc 'Build a development tarball from the commons git repo drush makefile (takes care of VM setup)'
task :build_commons_nightly do
  result = build_task('development')
  raise "Building the commons production tarball failed. Please check your logs." unless result
end

desc 'This will build a tarball from the current commons git master and boot a VM + install it'
task :launch_commons_dev_environment do
  if !ENV["COMMONS_TARBALL"].to_s.empty?
    puts "We seem to have a tarball specified: #{ENV["COMMONS_TARBALL"]}"
  else
    puts "We didn't find a COMMONS_TARBALL environment variable, so we're building a new nightly one."
    old_artifacts = Dir["#{ARTIFACTS_DIR}/*.tar.gz"]
    Rake::Task["build_commons_nightly"].execute
    ENV["COMMONS_TARBALL"] = created_artifact = (Dir["#{ARTIFACTS_DIR}/*.tar.gz"] - old_artifacts).sort.last
    puts "We will use this tarball in our dev environment: '#{ENV["COMMONS_TARBALL"]}'"
  end
  
  begin
    Rake::Task['boot_lamp_vm'].execute
    Rake::Task['install_commons_on_lamp_vm'].execute
    Rake::Task['install_qa_backdoor_into_vm'].execute
    puts "You should be able to access your commons site at http://localhost:5566/"
  rescue => e
    puts "Terminating the VM after setup failure: #{e.message}"
    Rake::Task["terminate_and_delete_vm"]
    raise e
  end
end

desc 'Launch a VM, install commons, inject the backdoor, run the tests, kill the vm'
task :launch_integration_tests do
  begin
    puts "Setting up commons dev environment to run tests in"
    Rake::Task['launch_commons_dev_environment'].execute
    puts "Running tests!"
    Dir.chdir("tests") do
      system('bundle exec cucumber --tags ~@wip --format pretty --format junit --out junit_reports')
    end
  ensure
    Rake::Task["terminate_and_delete_vm"]
  end
end
