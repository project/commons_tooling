Then /^I (?:should )?sleep (?:for )(\d+) seconds$/ do |naptime|
  sleep naptime.to_i
end

When /^I click (?:on )?["'](.*)['"]$/ do |link|
  click_link(link)
end

#The ?: tells us that we don't want to capture that part in a variable
When /^(?:I am|I'm|I) (?:on|viewing|looking at|look at|go to|visit|visiting) ['"]?([^"']*)["']?$/ do |path|
  translation_hash = {
    "the status report page" => '/admin/reports/status',

    "the blog posts page" => '/content/blogs',
    "the blog post page" => '/content/blogs',
    "the blogs page" => '/content/blogs',

    "the groups page" => '/groups',

    "the polls page" => '/content/polls',
    "the poll page" => '/content/polls',

    "the stream page" => '/stream',

    "the discussions page" => '/content/discussions',
    "the discussion page" => '/content/discussions',

    "the documents page" => '/content/documents',
    "the document page" => '/content/documents',

    'the wiki page' => '/content/wikis',
    'the events page' => '/content/events',
    'the site analytics page' => '/admin/reports/analytics',

    'the bookmarks page' => '/bookmarks',
  }
  cleaned_up_path = path.chomp.strip
  if translation_hash.key?(cleaned_up_path)
    visit(translation_hash[cleaned_up_path])
  else
    if cleaned_up_path.start_with?("/")
      visit(cleaned_up_path)
    else
      raise "I don't know how to go to this path: #{cleaned_up_path.inspect}."
    end

  end
end

And /the userpage of (.*) is visible/ do |user|
  visit('/users/')
  within(:css, 'div#view-id-user_directory-page_1') do
    click_link(user)
  end
end


Then /^there should be a button called ['"](.*)['"]$/ do |button_name|
  page.should have_button(button_name)
end

Then /^I should be on "(.*)"$/ do |path|
  current_path.should == path
end

Then /^I should (not )?see ['"]([^"]*)["']( within ['"]([^"]*)["'])?$/ do |presence, text, has_within, css_selector|
  # Select root HTML node if no selector is set
  css_selector ||= "html"
  within(css_selector) do
    if presence
      page.should have_no_content(text)
    else
      page.should have_content(text)
    end
  end
end

And /^I fill in ["']([^"'']*)["'] with ["'](.*)["']$/ do |field, content|
  page.fill_in field, :with => content
end

When /^I press ["']([^"]*)["'](?: in the "([^"]*)" section)?$/ do |element, scope|
  if scope
    within(scope) do
      click_button(element)  
    end
  else
    click_button(element)  
  end
end

Then /^all images should be present$/ do

  bad_results = []
  all(:xpath, "//img[@src]").each do |image_node|

    if image_node['src'].include?("http://")
      image_url = image_node['src']
    else
      #remove leading / and prepend the current_url
      image_url = "#{current_url.to_s.chomp("/")}/#{image_node['src'].reverse.chomp("/").reverse}"
    end

    #We can't check for logged in stuff
    next if image_url.include?('/admin/')

    http_status = Excon.head(image_url).status
    bad_results << {:http_status => http_status, :checked_url => image_url, :original_url => image_node['src'], :current_page => current_url.to_s} if http_status != 200
  end
  bad_results.should == []
end

Then /^there should be no links to (.*)$/ do |bad_link_target|
  found_links = page.all(:xpath, "//a[contains(@href, '#{bad_link_target}')]").map{|node| node['href'] }.uniq
  found_links.should == []
end

Given /^I have a new session$/ do
  begin
    Capybara.reset!
  rescue Errno::ECONNREFUSED, StandardError => e
    puts "Error while trying to reset the session: #{e.message}"
    puts "The current driver is: #{Capybara.current_driver}"
    puts "The default driver is: #{Capybara.default_driver}"
    puts "The website reposnds like this:\n#{`curl -I #{Capybara.app_host}`}"
    raise "Error while trying to reset the current session: #{e.message}"
  end
end

Given /^a fresh commons installation$/ do
  # Do snapshot reset
  step "I have a new session"
end

And /I enter the vertical tab ['"](.*)['"]/ do |tab_name|
  page.find(:xpath, "//div[contains(@class, 'vertical-tabs')]/.//a/strong[contains(text(), '#{tab_name}')]").click
end

And /I should see a message with the text ['"](.*)['"]/ do |text|
  page.find("div.messages.status").text.should include(text)
end

And /I should see a link with the text ['"](.*)['"]/ do |text|
  page.should have_xpath("//a[contains(text(), text)]")
end
And /I should see a search result with the text ['"](.*)['"]/ do |text|
  page.should have_xpath("//div[contains(@class, 'search-result')]/.//a[contains(text(), '#{text}')]")
end

And /^I should be on a page with the title ['"](.*)['"]$/ do |text|
  page.should have_xpath("/html/head/title[contains(text(), '#{text}')]")
end

And /^I choose ['"](.*)['"]$/ do |radio_button|
  page.choose(radio_button)
end

Then /^I should see the image ['"](.*)['"]$/ do |image_url|
  page.should have_css("img[src='#{image_url}']")
end

Then /^I should not see an error message$/ do
  page.all(:css, 'div.messages.error').map{|error| error.text }.should == []
end
