require 'rubygems'
require 'bundler/setup'
require 'excon'
require 'multi_json'
require 'uri'

Then /^I don't want to see an email with ["'](.*)['"] in the title$/ do |text|
  site_url = URI.parse(Capybara.app_host)
  mail_api_url = "#{site_url.scheme}://#{site_url.host}:5577/messages"
  data = Excon.get(mail_api_url).body
  parsed_data = MultiJson.load(data)
  parsed_data.select{|mail| mail['subject'].include?(text) }.should == []
end
