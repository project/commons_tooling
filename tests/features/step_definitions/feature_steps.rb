Then /^I don't want to see any overridden features$/ do
  backdoor = QaBackdoor.new("http://localhost:5566")
  feature_list = backdoor.list_features
  puts feature_list.inspect
  feature_list.should have_at_least(10).items
  # This is a bit awkward, but feature-list does not have pipe support
  # --> http://drupal.org/node/935932
  # This means that there will be linebreaks in the output.
  # "Overr" is what fits in the line
  feature_list.select{|feature| feature[:state].to_s.start_with? "Overr"}.should == []
end