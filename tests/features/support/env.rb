puts "loading env.rb"
require 'bundler/setup'
require 'capybara'
require 'capybara/dsl'
require 'capybara/poltergeist'
require 'excon'

Capybara.default_wait_time = 30

Capybara.register_driver :poltergeist do |app|
  options = {
    :js_errors => false,
    :window_size => [1280, 768],
    :timeout => 60,
  }
  Capybara::Poltergeist::Driver.new(app, options)
end

Capybara.default_driver = :poltergeist

#This is how we point the test at a certain domain
unless ENV['SUT_URL']
  puts "No SUT_URL environment variable found. We need this so we know which URL points to our system under test."
  exit 1
end

Capybara.app_host = ENV['SUT_URL'].chomp('/')

puts "Doing initial request to warm up caches"
cache_warming_url = Capybara.app_host + '/'
result = Excon.get(cache_warming_url)
puts "Initial request status code: #{result.status}"
if result.status.to_s != '200'
  puts "We didn't receive a 200 status code for the initial request to #{cache_warming_url}. Got: #{result.status}"
  puts "The Response headers:"
  puts result.headers.inspect
  puts "The HTML output looks as follows:"
  puts result.body.inspect
  exit(2)
end

puts "Enabling vagrant sandbox mode."
Dir.chdir('../vm_workspace'){ system('vagrant sandbox on') }

# If we tagged a step with @no_reset, we won't rollback the snapshot
# Otherwise we will
Before('~@no_reset') do
  puts "Resetting VM snapshot before starting the test"
  Dir.chdir('../vm_workspace'){ system('vagrant sandbox rollback') }
end

World(Capybara::DSL)