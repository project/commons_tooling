Feature: System setup
  @no_reset
  Scenario: As an admin, I do not want to have any overridden Commons features
    Then I don't want to see any overridden features
    
  @no_reset
  Scenario: As an admin, I do not want to receive an email about updates right after the installation
    Then I don't want to see an email with "New release(s) available" in the title