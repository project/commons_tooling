Feature: Homepage

  Scenario: A user wants to go to the homepage and actually find a working commons installation there
    Given I am viewing "/"
    Then I should see "Home"
    And I should see "Groups"
    And I should see "Events"

  Scenario: A user doesn't want any error messages on the default homepage
    Given I am viewing "/"
    Then I should not see an error message
