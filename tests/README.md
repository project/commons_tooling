#About
These are integration tests that simluate a user clicking through the major features of the Drupal Commons Distribution. The technical underpinnings are [Cucumber](http://cukes.info/) and [Capybara](http://jnicklas.github.com/capybara/), two Ruby libraries that provide a convenient API to describe the features Commons has in plain english and remote control a browser to test them.

These features are captured in ".feature" files within the "features" directory. Feel free to browse a bit arround and see what is already convered. The way to turn those plain-text features into actual code can be found in the "step_definitions" folder. The 'steps' provide the mapping from text to code. 


# Prerequisites

## Ruby
To be able to run the tests, you'll need a version of the Ruby virtual machine and the libraries ("gems") that we use. We'll use 'bundler' to take care of instlling the right versions of the libraries we're using and making sure that our tests will use those rather than other ones that might already be installed somewhere on the system.  
The steps to get your ruby environment set up are as follows:

1. Make sure Ruby is installed (1.8.7, but 1.9 will probably work too) 
2. Make sure the bundler gem is installed:  
> gem install bundler
3. Install all of the required gems  
(This has to happen in the dir that contains 'Gemfile'):  
> bundle install

If you happen to not have root access on the system you're using (e.g. a CI machine), you can install the gems into a local directory by using the '--path' switch and pointing it at a local directory you have write access to.

### Commons
As far as you commons installation goes, the only thing you should do is adding a little php file to your docroot. This will allow the tests to 'reset' you website to a clean state after clicking arround arround on it during a test. The PHP file will use git and the "mysql" commandline tool to take snapshots of the database and the filesystem of your drupal install.

To get your initial 'clean' snapshot, you should do this:

1. Copy instance_setup/testing_shortcuts/qa_reset.php to your docroot
2. Visit /qa_reset.php?operation=save_current_state to create a snapshot of your commons installation

## Running tests

1. Point the tests to the URL that your commons installation is reachable at:
> export SUT_URL=http://commons.dev.example.com/
2. To launch all of the tests (takes a while):
> bundle exec cucumber
3. To launch all tests for a secnario:  
>  bundle exec cucumber features/blog.feature
4. To launch a specific test (32 = line number of the test):  
>  bundle exec cucumber features/blog.feature:32

(In case you're wondering: the 'bundle exec' takes care of loading the previously installed versions of the libraries for us.)

Other fancy options you can pass to the cucumber app:  

- pretty console output while testing:
> --format pretty

- Don't run tests that are marked 'work in progress':  
> --tags ~@wip 

- Create an xml file that Jenkins can read:  
> --format junit --out junit_reports 

- Create a fancy HTML page with embedded screenshots for failures:  
> --format html --out test_results.html 